# Django Tutorials (Without Docker tools)

## Exercises (For Windows OS and VSCode Editor)

### Exercise.6

* How to use Django Admin dashboard to manage models?

__Login Admin dashboard__

> http://localhost:8000/admin/

__You should not see Question and Choice models__

__Adds Question and Choice models into Admin dashboard__

~~~
#Filename: polls/admin.py
from django.contrib import admin

from .models import Question, Choice

admin.site.register(Question)
admin.site.register(Choice)

~~~

__Refresh index page of dashboard and you should see Question and Choice models__

> http://localhost:8000/admin/


__Adjust appearance style of model__

~~~
#Filename: polls/admin.py
from django.contrib import admin

from .models import Choice, Question


# class ChoiceInline(admin.StackedInline):
class ChoiceInline(admin.TabularInline):
    model = Choice
    extra = 3


class QuestionAdmin(admin.ModelAdmin):
    fieldsets = [
        (None,               {'fields': ['question_text']}),
        ('Date information', {'fields': ['pub_date'], 'classes': ['collapse']}),
    ]
    inlines = [ChoiceInline]
    list_display = ('question_text', 'pub_date', 'was_published_recently')
    list_filter = ['pub_date']
    search_fields = ['question_text']

admin.site.register(Question, QuestionAdmin)

~~~

### Exercise.5

* How to adjust template style(e.g. css)?


__Try to surf your "index" path of Polls app on an browser__

> http://localhost:8000/polls/


__Create polls/static/polls folder__

> mkdir -p polls/static/polls

__Create polls/static/polls/style.css__

~~~
#Filename: polls/static/polls/style.css
li a {
    color: green;
}
~~~

__Set STATIC_ROOT in settings.py__

~~~
#Filename: dj_tutorial_prog/settings.py
...
STATIC_ROOT='static'
...
~~~

__Run collect static command__

> python manage.py collectstatic

__Edit polls/templates/polls/index.html template__

~~~
# Filename: polls/templates/polls/index.html
{% load static %}

<link rel="stylesheet" type="text/css" href="{% static 'polls/style.css' %}">

...

~~~

__Try to surf your "index" path of Polls app on an browser__

> http://localhost:8000/polls/


-----

### Exercise.4.4

* Amend View and URL as Django generic method

~~~
# Filename: polls/views.py

from .models import Question

from django.shortcuts import render
from django.shortcuts import get_object_or_404

from .models import Choice 
from django.http import HttpResponseRedirect
from django.urls import reverse

from django.views import generic

class IndexView(generic.ListView):
    template_name = 'polls/index.html'
    context_object_name = 'latest_question_list'

    def get_queryset(self):
        """Return the last five published questions."""
        return Question.objects.order_by('-pub_date')[:5]

class DetailView(generic.DetailView):
    model = Question
    template_name = 'polls/detail.html'


class ResultsView(generic.DetailView):
    model = Question
    template_name = 'polls/results.html'


def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        # This tip isn’t specific to Django;
        # it’s just good Web development practice.
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))
~~~

~~~
# Filename: polls/urls.py
from django.urls import path

from . import views

app_name = 'polls'

urlpatterns = [
    # ex: /polls/
    path('', views.IndexView.as_view(), name='index'),
    # ex: /polls/1/
    path('<int:pk>/', views.DetailView.as_view(), name='detail'),
    # ex: /polls/1/results/
    path('<int:pk>/results/', views.ResultsView.as_view(), name='results'),
    # ex: /polls/1/vote/
    path('<int:question_id>/vote/', views.vote, name='vote'),
]
~~~

------
### Exercise.4.3

* User(browser) send data to Database.

> Request Workflow: Browser(Vote data) --> urls.py-->views.py <--> models.py <--> Database (e.g. SQLite3 / PostgreSQL)

> Response Workflow: Browser <-- template <--views.py <--> models.py <--> Database (e.g. SQLite3 / PostgreSQL)

> App Workflow: questions list(index) <--> question detail(detail, html form) <--> vote(vote, redirect, no template) <--> vote result(result)

__Adds HTML form into detail.html template__
~~~
<!-- Filename: polls/templates/polls/detail.html -->
<h1>{{ question.question_text }}</h1>

{% if error_message %}<p><strong>{{ error_message }}</strong></p>{% endif %}

<!--Setting server side url-->
<form action="{% url 'polls:vote' question.id %}" method="post">
{% csrf_token %}
{% for choice in question.choice_set.all %}
    <input type="radio" name="choice" id="choice{{ forloop.counter }}" value="{{ choice.id }}">
    <label for="choice{{ forloop.counter }}">{{ choice.choice_text }}</label><br>
{% endfor %}
<input type="submit" value="Vote">
</form>

<a href="{% url 'polls:index'%}">Back to Question list</a>
~~~

__Try to request your "detail" path of Polls app on an browser__

> App Workflow: questions list(index) <--> question detail(detail, html form) <--> vote(vote, redirect, no template) <--> vote result(result)

> http://localhost:8000/polls/

__Create vote result template(results.html)__
~~~
<!--Filename: polls/templates/polls/results.html-->
<h1>{{ question.question_text }}</h1>

<ul>
{% for choice in question.choice_set.all %}
    <li>{{ choice.choice_text }} -- {{ choice.votes }} vote{{ choice.votes|pluralize }}</li>
{% endfor %}
</ul>

<a href="{% url 'polls:detail' question.id %}">Vote again?</a>
~~~

__Map vote result template to views and modify vote action rule in views__

> from .models import Choice

> from django.http import HttpResponseRedirect

> from django.urls import reverse

~~~
# Filename: polls/views.py

...

from .models import Choice
from django.http import HttpResponseRedirect
from django.urls import reverse

...

def results(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/results.html', {'question': question})

def vote(request, question_id):
    question = get_object_or_404(Question, pk=question_id)
    try:
        selected_choice = question.choice_set.get(pk=request.POST['choice'])
    except (KeyError, Choice.DoesNotExist):
        # Redisplay the question voting form.
        return render(request, 'polls/detail.html', {
            'question': question,
            'error_message': "You didn't select a choice.",
        })
    else:
        selected_choice.votes += 1
        selected_choice.save()
        # Always return an HttpResponseRedirect after successfully dealing
        # with POST data. This prevents data from being posted twice if a
        # user hits the Back button.
        # This tip isn’t specific to Django;
        # it’s just good Web development practice.
        return HttpResponseRedirect(reverse('polls:results', args=(question.id,)))

...
~~~


__Try to fill vote form and submit it__

> App Workflow: questions list(index) <--> question detail(detail, html form) <--> vote(vote, redirect, no template) <--> vote result(result)

> http://localhost:8000/polls/

-----

### Exercise.4.2
* Django URL name for hyperlink of template.

> Request Workflow: Browser(No Data) --> urls.py-->views.py <--> models.py <--> Database (e.g. SQLite3 / PostgreSQL)

> Response Workflow: Browser <-- template <--views.py <--> models.py <--> Database (e.g. SQLite3 / PostgreSQL)

> App Workflow: questions list(index) <--> question detail(detail) <--> vote(vote) <--> vote result(result)


__Create detail template__

~~~
<!-- Filename: polls/template/pollls/detail.html -->
<h1>{{ question.question_text }}</h1>
<h3>{{ question.pub_date}}</h3>
<ul>
{% for choice in question.choice_set.all %}
    <li>{{ choice.choice_text }}</li>
{% endfor %}
</ul>
~~~

__Modify detail view__

> from django.shortcuts import get_object_or_404

~~~
#Filename: polls/views.py
...
from django.shortcuts import get_object_or_404
...
def detail(request, question_id):
    # return HttpResponse("You're looking at question %s." % question_id)
    question = get_object_or_404(Question, pk=question_id)
    return render(request, 'polls/detail.html', {'question': question})
...
~~~

__Try to request your "detail" path of Polls app on an browser__

> App Workflow: questions list(index) <--> question detail(detail) <--> vote(vote) <--> vote result(result)

> http://localhost:8000/polls/


__Use app name(polls) instance of "/polls" path__

> app_name = 'polls'

~~~
# Filename: polls/urls.py
....
app_name = 'polls'
...
urlpatterns = [
    ...
]
~~~

> Path hyperlink: \<a href="/polls/{{ question.id }}/"\>

~~~
<!-- Filename: polls/templates/polls/index.html -->
{% if latest_question_list %}
    <ul>
    {% for question in latest_question_list %}
        <li><a href="/polls/{{ question.id }}/">{{ question.question_text }}</a></li>
    {% endfor %}
    </ul>
{% else %}
    <p>No polls are available.</p>
{% endif %}
~~~

> App Name hyperlink: \<a href="{% url 'polls:detail' question.id %}"\>

~~~
<!-- Filename: polls/templates/polls/index.html -->
{% if latest_question_list %}
    <ul>
    {% for question in latest_question_list %}
        <li><a href="{% url 'polls:detail' question.id %}">{{ question.question_text }}</a></li>
    {% endfor %}
    </ul>
{% else %}
    <p>No polls are available.</p>
{% endif %}
~~~

__Try to request your "detail" path of Polls app on an browser__

> App Workflow: questions list(index) <--> question detail(detail) <--> vote(vote) <--> vote result(result)

> http://localhost:8000/polls/


---------
### Exercise.4.1
* Django Views(*.py)
* Django URLs(*.py)
* Django Templates(*.html)
* Django Request/Response workflow
* Polls App workflow

__Django request/response workflow__

> Request Workflow: Browser(No data) --> urls.py-->views.py <--> models.py <--> Database (e.g. SQLite3 / PostgreSQL)

> Response Workflow: Browser <-- HttpResponse <--views.py <--> models.py <--> Database (e.g. SQLite3 / PostgreSQL)

> App Workflow: questions list(index) <--> question detail(detail) <--> vote(vote) <--> vote result(result)

__Modify polls/views.py__
~~~
#Filename: polls/views.py
from django.http import HttpResponse
from .models import Question

def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    output = ', '.join([q.question_text for q in latest_question_list])
    return HttpResponse(output)

def detail(request, question_id):
    return HttpResponse("You're looking at question %s." % question_id)

def results(request, question_id):
    response = "You're looking at the results of question %s."
    return HttpResponse(response % question_id)

def vote(request, question_id):
    return HttpResponse("You're voting on question %s." % question_id)
~~~

__Create and modify polls/urls.py__
~~~
# Filename: polls/urls.py
from django.urls import path

from . import views

urlpatterns = [
    # ex: /polls/
    path('', views.index, name='index'),
    # ex: /polls/1/
    path('<int:question_id>/', views.detail, name='detail'),
    # ex: /polls/1/results/
    path('<int:question_id>/results/', views.results, name='results'),
    # ex: /polls/1/vote/
    path('<int:question_id>/vote/', views.vote, name='vote'),
]
~~~

__Adds poll app URLs into dj_tutorial_proj__

~~~
# Filename: dj_tutorial_prog/urls.py

...
from django.urls import include
...

urlpatterns = [
    ...
    path('polls/', include('polls.urls')),
    ...
]
~~~

__Try to request your django URLs on an browser__

> http://localhost:8000/polls/

> http://localhost:8000/polls/1/

> http://localhost:8000/polls/1/results/

> http://localhost:8000/polls/1/vote/

__Django Template(not pure HTML & includes django tag) Role__

> Request Workflow: Browser(No data) --> urls.py-->views.py <--> models.py <--> Database (e.g. SQLite3 / PostgreSQL)

> Response Workflow: Browser <-- template / HttpResponse<--views.py <--> models.py <--> Database (e.g. SQLite3 / PostgreSQL)

> App Workflow: questions list(index) <--> question detail(detail) <--> vote(vote) <--> vote result(result)

__Create templates folder for polls app__

> Linux-like OS Path: polls/templates/pollsdddddddddddddd

> Windows-like OS Path: polls\templates\polls

__Create "index.html" template for index URLs of polls app__

> Linux-like OS Path: polls/templates/polls/index.html

> Windows-like OS Path: polls\templates\polls\index.html

__Modify "index.html" template__

~~~
<!-- Filename: polls/templates/polls/index.html -->
{% if latest_question_list %}
    <ul>
    {% for question in latest_question_list %}
        <li><a href="/polls/{{ question.id }}/">{{ question.question_text }}</a></li>
    {% endfor %}
    </ul>
{% else %}
    <p>No polls are available.</p>
{% endif %}
~~~

__Uses render(for Django template) instance of HttpResponse in polls/views.py__

> from django.shortcuts import render

> return render(request, 'polls/index.html', context)

~~~
...
from django.shortcuts import render
...
def index(request):
    latest_question_list = Question.objects.order_by('-pub_date')[:5]
    context = {'latest_question_list': latest_question_list}
    # return HttpResponse(output)
    return render(request, 'polls/index.html', context)
~~~

__Try to request your "index" path of Polls app on an browser__

> App workflow: questions list(index) <--> question detail(detail) <--> vote(vote) <--> vote result(result)

> http://localhost:8000/polls/

---------

### Exercise.3
* Change Database from SQLite3 to PostgreSQL
* Django app
* Django models operations
* Django Shell
* Use Django Extensions for showing raw SQL statement
* Manage models on Django dashboard.


__View django_poll database in PGAdmin4__
> Run PGAdmin4 -> click "django_poll" database and check no table in django_poll db.  

__Change DB configuration in settings.py__
~~~
# File: dj_tutorial_prog/settings.py
...
DATABASES = {
    'default': {
        'ENGINE': 'django.db.backends.postgresql_psycopg2',
        'NAME': 'django_polls',
        'USER': 'postgres',
        'PASSWORD': '1234',
        'HOST': '127.0.0.1',
        'PORT': '5432',
    }
}
...
~~~

__Re-migrate Django DB__

> python manage.py makemigrations

> python manage.py migrate


__View django_poll database in PGAdmin4__
> Run PGAdmin4 -> click "django_polls" database and check django tables in django_polls db.

__Create your super user of Django__

> python manage.py createsuperuser

~~~
Username (leave blank to use 'willis'): willis
Email address: misweyu2007@gmail.com
Password: 
Password (again): 
The password is too similar to the username.
Bypass password validation and create user anyway? [y/N]: y
Superuser created successfully.
~~~

__Check super user with Admin dashboard and PGAdmin4__

> http://localhost:8000/admin/

> Run PGAdmin4

__Create Django app - polls__

> Django app name: polls

> python manage.py startapp polls

__View file structure of Django app__

> ls C:\django-workspace\polls

__Adds polls app into dj_tutorial_prog project__

~~~

#Filename: dj_tutorial_prog/settings.py
...
INSTALLED_APPS = [
    'django.contrib.admin',
    'django.contrib.auth',
    'django.contrib.contenttypes',
    'django.contrib.sessions',
    'django.contrib.messages',
    'django.contrib.staticfiles',

    'polls.apps.PollsConfig'
]
...

~~~

__Adds Question and Choice models into polls app__
~~~
# polls/models.py
from django.db import models


class Question(models.Model):
    question_text = models.CharField(max_length=200)
    pub_date = models.DateTimeField('date published')


class Choice(models.Model):
    question = models.ForeignKey(Question, on_delete=models.CASCADE)
    choice_text = models.CharField(max_length=200)
    votes = models.IntegerField(default=0)

~~~

__Make migration for Question and Choice models__

> python manage.py makemigrations

~~~
(env) PS C:\django-workspace> python .\manage.py makemigrations
Migrations for 'polls':
  polls\migrations\0001_initial.py
    - Create model Question
    - Create model Choice
~~~

__Check difference after making migration__

> ls polls/migrations

__What do "Make migrations" do?__

>  python manage.py sqlmigrate polls 0001

~~~
BEGIN;
--
-- Create model Question
--
CREATE TABLE "polls_question" ("id" serial NOT NULL PRIMARY KEY, "question_text" varchar(200) NOT NULL, "pub_date" timestamp with time zone NOT NULL);
--
-- Create model Choice
--
CREATE TABLE "polls_choice" ("id" serial NOT NULL PRIMARY KEY, "choice_text" varchar(200) NOT NULL, "votes" integer NOT NULL, "question_id" integer NOT NULL);
ALTER TABLE "polls_choice" ADD CONSTRAINT "polls_choice_question_id_c5b4b260_fk_polls_question_id" FOREIGN KEY ("question_id") REFERENCES "polls_question" ("id") DEFERRABLE INITIALLY DEFERRED;
CREATE INDEX "polls_choice_question_id_c5b4b260" ON "polls_choice" ("question_id");
COMMIT;
~~~

__Creates DB tables according Django migrations files__

> python manage.py migrate

__View question and choice tables in PGAdmin4__

> Run PGAdmin4 -> see polls_choice and polls_question tables

__Uses interactive Django Shell to practice Django models__

> python manage.py shell
~~~

Python 3.7.3 (tags/v3.7.3:ef4ec6ed12, Mar 25 2019, 22:05:12) [MSC v.1916 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>>

~~~

__Query data of Django model__

~~~
(env) PS C:\django-workspace> python manage.py shell
Python 3.7.3 (tags/v3.7.3:ef4ec6ed12, Mar 25 2019, 22:05:12) [MSC v.1916 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> from polls.models import Choice, Question
>>> Question.objects.all()
<QuerySet []>
>>> q = Question.objects.all()
>>> print(q.query)
SELECT "polls_question"."id", "polls_question"."question_text", "polls_question"."pub_date" FROM "polls_question"
~~~

__Create data of Django model__
~~~
(env) PS C:\django-workspace> python manage.py shell
Python 3.7.3 (tags/v3.7.3:ef4ec6ed12, Mar 25 2019, 22:05:12) [MSC v.1916 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> from polls.models import Choice, Question
>>> from django.utils import timezone
>>> q = Question(question_text="What's new?", pub_date=timezone.now())
>>> q.save()
~~~

__Query question data with PGAdmin4__

> Run PGAdmin4 > query polls_question table.


__Install Django Extension package__


> Adds "django-extension" to "requirements.txt" file and INSTALLED_APPS variable

~~~
# django-workspace\requirements.txt
...
# Development
django-extension
~~~

~~~
# Filename: settings.py
INSTALLED_APPS = [
    ...
    'django_extensions',
    ...
]
~~~


> pip install -r requirements.txt

__Enable "Print SQL" Django shell mode__

> python .\manage.py shell_plus --print-sql

~~~

# Shell Plus Model Imports
from django.contrib.admin.models import LogEntry
from django.contrib.auth.models import Group, Permission, User
from django.contrib.contenttypes.models import ContentType
from django.contrib.sessions.models import Session
from polls.models import Choice, Question
# Shell Plus Django Imports
from django.core.cache import cache
from django.conf import settings
from django.contrib.auth import get_user_model
from django.db import transaction
from django.db.models import Avg, Case, Count, F, Max, Min, Prefetch, Q, Sum, When, Exists, OuterRef, Subquery
from django.utils import timezone
from django.urls import reverse
Python 3.7.3 (tags/v3.7.3:ef4ec6ed12, Mar 25 2019, 22:05:12) [MSC v.1916 64 bit (AMD64)] on win32
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)

~~~



__Show raw SQL statement for Django model save/update operation__
~~~

(InteractiveConsole)
>>> from polls.models import Choice, Question
>>> from django.utils import timezone
>>> q = Question(question_text="What's new? Enable raw SQL mode", pub_date=timezone.now())
>>> q.save()
INSERT INTO "polls_question" ("question_text", "pub_date")
VALUES ('What''s new? Enable raw SQL mode', '2019-05-22T09:46:20.709998+00:00'::timestamptz) RETURNING "polls_question"."id"

Execution time: 0.010715s [Database: default]

>>> q.question_text = "What's new? Enable raw SQL mode - Update"
>>> q.save()
UPDATE "polls_question"
   SET "question_text" = 'What''s new? Enable raw SQL mode - Update',
       "pub_date" = '2019-05-22T09:46:20.709998+00:00'::timestamptz
 WHERE "polls_question"."id" = 3

Execution time: 0.009717s [Database: default]

>>> q.id
3
>>> q.question_text
"What's new? Enable raw SQL mode - Update"
>>> q.pub_date
datetime.datetime(2019, 5, 22, 9, 46, 20, 709998, tzinfo=<UTC>)

~~~


__Django model query operation__

~~~
(InteractiveConsole)
>>> from polls.models import Choice, Question
>>> from django.utils import timezone
>>> Question.objects.all()
SELECT "polls_question"."id",
       "polls_question"."question_text",
       "polls_question"."pub_date"
  FROM "polls_question"
 LIMIT 21

Execution time: 0.001057s [Database: default]

<QuerySet [<Question: Question object (1)>, <Question: Question object (2)>, <Question: Question object (3)>]>

>>> qlist = Question.objects.all()
>>> qlist[0].question_text
SELECT "polls_question"."id",
       "polls_question"."question_text",
       "polls_question"."pub_date"
  FROM "polls_question"
 LIMIT 1

Execution time: 0.001158s [Database: default]

"What's new?"
>>> qlist[2].question_text
SELECT "polls_question"."id",
       "polls_question"."question_text",
       "polls_question"."pub_date"
  FROM "polls_question"
 LIMIT 1
OFFSET 2

Execution time: 0.000000s [Database: default]

"What's new? Enable raw SQL mode - Update"
>>> Question.objects.all()
SELECT "polls_question"."id",
       "polls_question"."question_text",
       "polls_question"."pub_date"
  FROM "polls_question"
 LIMIT 21

Execution time: 0.004074s [Database: default]

<QuerySet [<Question: Question object (1)>, <Question: Question object (2)>]>
>>> 

~~~

__Do you confuse "<QuerySet [<Question: Question object (1)>, ...]>"?__

~~~
Filename: polls/models.py

import datetime
from django.db import models
from django.utils import timezone
...

class Question(models.Model):
    ...
    def __str__(self):
        return self.question_text

    def was_published_recently(self):
        return self.pub_date >= timezone.now() - datetime.timedelta(days=1)
    ...

class Choice(models.Model):
    ...
    def __str__(self):
        return self.choice_text
    
~~~

~~~
Python 3.6.7 (default, Oct 22 2018, 11:32:17) 
[GCC 8.2.0] on linux
Type "help", "copyright", "credits" or "license" for more information.
(InteractiveConsole)
>>> Question.objects.all()
SELECT "polls_question"."id",
       "polls_question"."question_text",
       "polls_question"."pub_date"
  FROM "polls_question"
 LIMIT 21

Execution time: 0.002423s [Database: default]

<QuerySet [<Question: What's new?>, <Question: What's new? Enable raw SQL mode - Update>]>
~~~

__Models common commands__

~~~
(InteractiveConsole)

>>> Question.objects.filter(id=1)
SELECT "polls_question"."id",
       "polls_question"."question_text",
       "polls_question"."pub_date"
  FROM "polls_question"
 WHERE "polls_question"."id" = 1
 LIMIT 21

Execution time: 0.001387s [Database: default]

<QuerySet [<Question: What's new?>]>
~~~


~~~
(InteractiveConsole)

>>> Question.objects.filter(question_text__startswith='What')
SELECT "polls_question"."id",
       "polls_question"."question_text",
       "polls_question"."pub_date"
  FROM "polls_question"
 WHERE "polls_question"."question_text"::text LIKE 'What%'
 LIMIT 21

Execution time: 0.002035s [Database: default]

<QuerySet [<Question: What's new?>, <Question: What's new? Enable raw SQL mode - Update>]>
~~~

~~~
(InteractiveConsole)

>>> Question.objects.filter(question_text__startswith='What')
SELECT "polls_question"."id",
       "polls_question"."question_text",
       "polls_question"."pub_date"
  FROM "polls_question"
 WHERE "polls_question"."question_text"::text LIKE 'What%'
 LIMIT 21

Execution time: 0.002035s [Database: default]

<QuerySet [<Question: What's new?>, <Question: What's new? Enable raw SQL mode - Update>]>
~~~

~~~
(InteractiveConsole)
>>> from django.utils import timezone
>>> current_year = timezone.now().year
>>> Question.objects.filter(pub_date__year=current_year)
SELECT "polls_question"."id",
       "polls_question"."question_text",
       "polls_question"."pub_date"
  FROM "polls_question"
 WHERE "polls_question"."pub_date" BETWEEN '2019-01-01T00:00:00+00:00'::timestamptz AND '2019-12-31T23:59:59.999999+00:00'::timestamptz
 LIMIT 21

Execution time: 0.001661s [Database: default]

<QuerySet [<Question: What's new?>, <Question: What's new? Enable raw SQL mode - Update>]>
~~~


~~~
(InteractiveConsole)
>>> Question.objects.get(id=2)
>>> Question.objects.get(id=2)
SELECT "polls_question"."id",
       "polls_question"."question_text",
       "polls_question"."pub_date"
  FROM "polls_question"
 WHERE "polls_question"."id" = 2

Execution time: 0.001449s [Database: default]

<Question: What's new? Enable raw SQL mode - Update>
~~~

~~~
(InteractiveConsole)
>>> Question.objects.get(id=2)
SELECT "polls_question"."id",
       "polls_question"."question_text",
       "polls_question"."pub_date"
  FROM "polls_question"
 WHERE "polls_question"."id" = 2

Execution time: 0.001449s [Database: default]

<Question: What's new? Enable raw SQL mode - Update>

>>> q = Question.objects.get(id=2)
SELECT "polls_question"."id",
       "polls_question"."question_text",
       "polls_question"."pub_date"
  FROM "polls_question"
 WHERE "polls_question"."id" = 2

Execution time: 0.001463s [Database: default]

>>> q.was_published_recently()
True
~~~

__Adds choice in specific queuestion__

~~~
(InteractiveConsole)
>>> q.choice_set.all()
SELECT "polls_choice"."id",
       "polls_choice"."question_id",
       "polls_choice"."choice_text",
       "polls_choice"."votes"
  FROM "polls_choice"
 WHERE "polls_choice"."question_id" = 1
 LIMIT 21

Execution time: 0.002868s [Database: default]

<QuerySet []>

>>> q.choice_set.create(choice_text='Not much', votes=0)
INSERT INTO "polls_choice" ("question_id", "choice_text", "votes")
VALUES (1, 'Not much', 0) RETURNING "polls_choice"."id"

Execution time: 0.004611s [Database: default]

<Choice: Not much>

>>> q.choice_set.create(choice_text='The sky', votes=0)
INSERT INTO "polls_choice" ("question_id", "choice_text", "votes")
VALUES (1, 'The sky', 0) RETURNING "polls_choice"."id"

Execution time: 0.003825s [Database: default]

<Choice: The sky>

>>> c = q.choice_set.create(choice_text='Just hacking again', votes=0)
INSERT INTO "polls_choice" ("question_id", "choice_text", "votes")
VALUES (1, 'Just hacking again', 0) RETURNING "polls_choice"."id"

Execution time: 0.004120s [Database: default]

>>> c.question
<Question: What's new?>

<Question: What's new?>
>>> q.choice_set.all()
SELECT "polls_choice"."id",
       "polls_choice"."question_id",
       "polls_choice"."choice_text",
       "polls_choice"."votes"
  FROM "polls_choice"
 WHERE "polls_choice"."question_id" = 1
 LIMIT 21

Execution time: 0.000634s [Database: default]

<QuerySet [<Choice: Just hacking again>, <Choice: The sky>, <Choice: Not much>]>
>>> q.choice_set.count()
SELECT COUNT(*) AS "__count"
  FROM "polls_choice"
 WHERE "polls_choice"."question_id" = 1

Execution time: 0.001558s [Database: default]

3
~~~

---------

### Exercise.2
* Uses SQLite3 Explorer in VSCode. 
* Create Djagno project - dj_tutorial_prog
* DB migration
* Start Django service
* Create super user
* Login Admin dashboard
* View SQLite3 table of Django with SQLite3 Explorer

__Activate python virtual environment__
> cd C:\django-workspace

> .\env\Scripts\activate

__Create Django project "dj_tutorial_prog"__
> cd C:\django-workspace

> django-admin startproject dj_tutorial_prog .

> ls C:\django-workspace

> python manage.py makemigrations

> python manage.py migrate

__View SQLites3__

> Right click "db.sqlite3" ->  Open Database -> SQLite Explorer

__Run Django service__

> python manage.py runserver 0:8000

__Allow 8000 port for Windows firewall__

> Pop "Windows Security Alert" dialog window -> Click "Allow access"

__Try to surf your django website__

> http://localhost:8000

__Invite your classmate to your website__

> ifconfig -> your IPv4 Address

> http://{your IPv4 address}:8000/

__Administrator Login__

> http://localhost:8000/admin

__Create your super user of Django__

> python manage.py createsuperuser

~~~
Username (leave blank to use 'willis'): willis
Email address: misweyu2007@gmail.com
Password: 
Password (again): 
The password is too similar to the username.
Bypass password validation and create user anyway? [y/N]: y
Superuser created successfully.
~~~

__Login Administrator dashboard with your super user__

> http://localhost:8000/admin

__View your super user data in SQLite3__

> Use SQLite explorer

> Right click "auth_user" table -> Show Table

---------

### Exercise.1
* Install PostgreSQL
* Install Git
* Install Visual Studio Code(VSCode)
* Install Python
* How to develope Django with VSCode
* What is virtualenv?
* Install Python and Django packages

__Install PostgreSQL && PGAdmin4__

> https://www.enterprisedb.com/downloads/postgres-postgresql-downloads

__Run PGAdmin4__

> Windows Start > search PGAdmin4 > click PGAdmin4

__Create django db with PGAdmin4__

> DB name: django_poll

__Install VSCode(Editor) and its Gitlens plugin__

> https://code.visualstudio.com/download

__Install Git__

> https://git-scm.com/download/win

__Check Git version__

> git --version

__Download tutorial Git repository__

> cd C:\

> git clone https://gitlab.com/misweyu2007/django-tutorials-app-by-steps-without-docker.git

__Import "django-tutorials-app-by-steps-without-docker" to VSCode__


__Install Python__

> https://www.python.org/downloads/windows/

__Check Python version with VSCode terminal__

> python --version

__Install VirtualEnv with pip__

> python -m pip install --upgrade pip --user

> pip install virtualenv

__Create and import "django-workspace" with VSCode__

> cd C:\

> mkdir -p django-workspace

> cd C:\django-workspace


__Creates Python virtual environment__

> cd C:\django-workspace

> python3 -m venv env

> env/Scripts/activate

> deactivate

> env/Scripts/activate

> python -m pip install --upgrade pip

__Create and install python packages list for Django project__

> cd C:\django-workspace

> creates package list ("django-workspace\requirements.txt")

~~~
# django-workspace\requirements.txt

# Django
Django>=2.0,<3.0

# PostgreSQL
psycopg2>=2.7,<3.0
~~~

> pip install -r .\requirements.txt

__Check packages folder path__

> ls .\env\Lib\site-packages\

__Check django version__

> python -m django --version

---------

## What is the repository for?

Know how to use Django.